# Tabu Search

Jest to jesdna spośród popularnych metod wyszukiwania lokalnego,
wykorzystująca przeczesywanie najbliższych elementów przestrzeni (sąsiadów)
i kilka dodatkowych zasad, mających powstrzymać algorytm od zatrzymywania się
w minimach lokalnych - listę Tabu, od któ©ej pochodzi nazwa algorytmu.

Proces przeszukiwania metodą Tabu Sarch rozpoczyna się od wybrania 
dowolnej poprawnej trasy początkowej - może ona być wylosowana bądź dobrana
za pomocą heurystyki. Zapamiętywana jest ona jako *najlepsza trasa*.

Nastepnym krokiem jest przeszukanie przestrzeni najbliższych
sąsiadów - czyli rozwiązań bliskim obecnie rozpatrywanemu.

Generowanie sąsiadów wykonywane jest za pomocą ruchów, przykładowe ruchy to:

* Zamiana i-tego wierzchołka z wierzchołkiem j-tym
* Wstawienie i-tego wierzchołka na j-te miejsce
* Obrócenie kolejności wierzchołków rozpoczynając od wierzchołka i, kończąc na j

Ruch musi być możliwy do zapisania w jednoznacznej formie, na przykład: dla zamiany
i-tego wierzchhołka z j-tym konieczne byłby zapisanie liczb i oraz j.

Po wygenereowaniu wszystkich możliwych sąsiadów za pomocą wybranej metody
dokonywania ruchów, wybierany jest najlepszy z nich, tj ten, dla którego
trasa drogi jest jak najkrótsza.

Ruch, który doprowadził do wygenerowania najlepszego sąsiada, zapisywany
jest do lisrt Tabu, co nie pozwoli na wykonywanie go przez kilka następnych 
iteracji algorytmu, natomiast najlepszy sąsiad zapisywany jest jako
rozwiązanie, od którego generowane będą kolejne.

Jeśli uzyskane rozwiązanie lepsze jest od zapamiętanego globalnego rozwiązania,
nowe rozwiązanie wpisywane jest w miejsce starego, gorszego. 
