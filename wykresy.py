from numpy import genfromtxt
import numpy as np
import matplotlib.pyplot as plt

data_17 = genfromtxt('17.result', delimiter=',')
data_55 = genfromtxt('55.result', delimiter=',')
data_170 = genfromtxt('170.result', delimiter=',')


np.set_printoptions(suppress=True)
a = data_17[:, 1]
b = data_55[:, 1]
c = data_170[:, 1]

print(c[::2])

exit()

plt.plot(data_17[:, 0], data_17[:, 1], label="tsp_17")
plt.plot(data_55[:, 0], data_55[:, 1], label="tsp_55")
plt.plot(data_170[:, 0], data_170[:, 1], label="tsp_170")

plt.legend()
plt.xlabel("Czas działania [ms]")
plt.ylabel("Błąd względny [%]")
plt.xscale('log')
plt.show()

# print(my_data)
