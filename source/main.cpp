#include <iostream>
#include "../include/Timer.h"
#include "../include/ResourceMgr.h"
#include "../include/OutputMgr.h"
#include "../include/BBSolver.h"
#include "../include/TSSolver.h"
#include "../include/DPSolver.h"
#include <string>
#include <vector>
#include <filesystem>
#include <cmath>

// #include <iomanip> // std::setprecision

using namespace std;
namespace fs = std::filesystem;

arrType current_graph;
string current_graph_name = "null";
vector<string> available_files;

Timer timer;
DPSolver dp;
BBSolver bb;
TSSolver ts;

void get_available_files()
{
    available_files.clear();

    std::string path = "./resources";
    for (const auto &entry : fs::directory_iterator(path))
        available_files.push_back(entry.path());
}

void print_avialable_files()
{
    for (int i = 0; i < available_files.size(); i++)
    {
        cout << i << ": " << available_files[i] << endl;
    }
}

void display_menu()
{
    cout << "==========================" << endl;
    cout << "Obecnie załadowany graf: " << current_graph_name << endl;
    cout << "==========================" << endl;
    cout << "w: Wczytaj graf z pliku" << endl;
    cout << "g: Wygeneruj graf losowo" << endl;
    cout << "p: Pokaż graf" << endl;
    cout << "t: Rozwiąż TSP za pomocą Tabu Search" << endl;
    cout << "d: Rozwiąż TSP za pomocą Dynamic Programming" << endl;
    cout << "b: Rozwiąż TSP za pomocą Branch & Bound" << endl;
    cout << "e: Wyjdź" << endl;

    cout << "==========================" << endl;
}

char get_choice()
{
    char c;
    cin >> c;

    return c;
}

void handle_choice(char c)
{

    switch (c)
    {

    case 'w':
        get_available_files();
        print_avialable_files();

        cout << "numer pliku: ";
        int choice;
        cin >> choice;

        current_graph_name = available_files[choice];
        current_graph = ResourceMgr::readGraphFromFile(available_files[choice]);
        break;

    case 'g':
    int size;
    cout << "Podaj liczbę miast: ";
    cin >> size;
    
    current_graph_name = "generated_" + to_string(size);
    current_graph = ResourceMgr::generateRandomGraph(size);
    break;

    case 'p':
    OutputMgr::showGraph(current_graph);
    break;

    case 'e':
    exit(0);
    break;

    case 't':

    int time_micros;
    cout << "Podaj czas pracy w mikrosekundach: ";
    cin >> time_micros;

    ts.max_time_microseconds = time_micros;

    timer.start();
    ts.solve(current_graph);
    timer.stop();
    ts.display_result();
    timer.printTimeInSeconds();
    break;
    
    case 'd':
    timer.start();
    dp.solve(current_graph);
    timer.stop();
    dp.display_result();
    timer.printTimeInSeconds();
    break;
    
    case 'b':
    timer.start();
    bb.solve(current_graph);
    timer.stop();
    bb.display_result();
    timer.printTimeInSeconds();
    break;
    
    default:
        break;
    }
}

void menu_loop()
{
    while (!0)
    {
        display_menu();
        char c = get_choice();
        handle_choice(c);
    }
}

int main(int argc, char **argv)
{

    if (argc == 1)
    {
        menu_loop();

        return 0;
    }


    /*
    auto graph = ResourceMgr::readGraphFromFile("./resources/tsp_170.txt");
    int best_sol =  2755;


    TSSolver t;

    int i = 1;
    while (i < 10000000) {

        t.max_time_microseconds = i;
        t.solve(graph);

        cout << i << "," << (float)abs(t.min_cost - best_sol) / (float)best_sol << endl;

        if(i == 1) {
            i = 2;
        } else {
            i *= 2;
        }
    }
    */

   DPSolver dp;
   TSSolver ts;

    // Tested:
    // * 5-15
    // * 16-20
    // * 21-25
   int PROBLEM_SIZE_MIN = 21;
   int PROBLEM_SIZE_MAX = 25;

   int TIME_FOR_SOLUTION_MIN = 2;
   int TIME_FOR_SOLUTION_MAX = 1000000;

   int ITERATIONS = 5;

   for (int sizes = PROBLEM_SIZE_MIN; sizes < PROBLEM_SIZE_MAX; sizes++)
   {
       for(int time_for_solution = TIME_FOR_SOLUTION_MIN; time_for_solution < TIME_FOR_SOLUTION_MAX; time_for_solution*=2) {

           float accuracy = 0;
           for(int i = 1; i < ITERATIONS+1; i++) {

                auto graph = ResourceMgr::generateRandomGraph(sizes);

                dp.solve(graph);
                int accurate_solution = dp.min_cost;

                ts.max_time_microseconds = time_for_solution;
                ts.solve(graph);
                int tabu_solution = ts.min_cost;

                accuracy += (float)abs(accurate_solution - tabu_solution) / (float)accurate_solution;
           }

           accuracy/=(float)ITERATIONS;
           cout << accuracy << ",";
       }

       cout << endl;
   }
   
}
