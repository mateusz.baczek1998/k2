#include "../include/Timer.h"

void Timer::stop()
{
    begin = std::chrono::steady_clock::now();
}

void Timer::start()
{
    end = std::chrono::steady_clock::now();
}

double Timer::getTimeInMicroSec()
{
    return std::chrono::duration_cast<std::chrono::microseconds>(begin - end).count();
}

void Timer::printTimeInSeconds()
{
    std::cout << "# " << std::setprecision(17) << std::fixed << getTimeInMicroSec() / 1000000.0 << std::endl;
    // std::cout << "# " /*<< std::setprecision(1)*/ << getTimeInMicroSec() << std::endl;
}