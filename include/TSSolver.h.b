#ifndef TS_SOLVER
#define TS_SOLVER

#include "./ResourceMgr.h"
#include <iostream>
#include <vector>
#include <algorithm>
#include <iomanip>
#include<bits/stdc++.h> 
#include "./solver.h"

#define TABU_LIST_ELEMENT_LIFETIME 10

class TabuElement {

public:
    int i,j; // Move parameters
    
    bool match(int other_i, int other_j) {
        if( i == other_i && j == other_j)
            return true;

        if( j == other_i && i == other_j)
            return true;

        return false;
    }

};

class TabuSolution {
    public:
        TabuElement tabuElement;
        vector<int> solution;

    TabuSolution(vector<int> &parent, int swap_i, int swap_j) {

        this->solution = parent;

        std::swap(
            this->solution[swap_j],
            this->solution[swap_i]
        );

        this->tabuElement = TabuElement(/*swap_j, swap_i*/);
        this->tabuElement.i = swap_i;
        this->tabuElement.j = swap_j;
        
    }  
};

class TSSolver : public Solver
{
public:
    void solve(arrType map);
    int calculate_cost(vector<int> &path);
    bool stoppingCondition();

    int max_iterations;
    deque<TabuElement> tabuList;

    bool onTabuList(int i, int j);

    vector<TabuSolution> get_neighbours(vector<int> &path);

};



#endif