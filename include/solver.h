#ifndef SOLVER
#define SOLVER

// #include "../source/utils.cpp"

#include "./ResourceMgr.h"

class Solver {
    public:

    // Variables needed for every algorithm
    arrType map;

    int min_cost;
    std::vector<int> best_path;

    // The method thats unique for every algorithm
    virtual void solve(arrType map) = 0; 


    // Additional utils
    void print(const std::vector<int> &v)
    {
        for (int e : v)
        {
            std::cout << " " << e;
        }
        std::cout << std::endl;
    }

    void display_result() {
        std::cout << "Cost is: " << min_cost << std::endl;
        print(best_path);
    }

};

#endif