#ifndef TIMER_H
#define TIMER_H

#include<chrono>
#include <iostream>
#include <iomanip>

using timePoint = std::chrono::steady_clock::time_point;

class Timer
{
    private:

    timePoint begin;
    timePoint end;

    public:
    Timer() = default;


    void stop();
    void start();
    double getTimeInMicroSec();

    void printTimeInSeconds();
};

#endif