#ifndef DP_SOLVER
#define DP_SOLVER

#include "./ResourceMgr.h"
#include <iostream>
#include <vector>
#include <algorithm>
#include <iomanip>
#include <bits/stdc++.h>
#include "./solver.h"
#include <math.h>
#include <bitset>

struct DPPartialSolution
{
    int id;
    int cost;
};

class DPSolver : public Solver
{
public:

    vector<vector<int>> connections;
    //vector<vector<int>> paths;
    int num_of_cities;
    long power_problem_size;

    void solve(arrType map);

    // void display_result();
    std::vector<DPPartialSolution> solutions;

    int recurring_tsp_step(int start, long set);

    int count_ones_in_binary_representation(int value);
    int count_zeros_in_binary_representation(int value);
    void restore_path();
};

#endif